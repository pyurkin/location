<?php

namespace LocationBundle\Exception;

use Curl\Curl;

class ValidationResponseException extends \Exception
{
    /**
     * @param Curl $client
     * @param array $validationErrors
     * @param \Throwable|null $previous
     */
    public function __construct(Curl $client, array $validationErrors, \Throwable $previous = null)
    {
        parent::__construct(sprintf('Response error: "%s" (url "%s")', implode(', ', $validationErrors), $client->url), 0, $previous);
    }
}