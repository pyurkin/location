<?php

namespace LocationBundle\Exception;

use LocationBundle\Model\Error;

class ErrorResponseException extends \Exception
{
    /**
     * @param Error $error
     * @param \Throwable|null $previous
     */
    public function __construct(Error $error, \Throwable $previous = null)
    {
        parent::__construct(sprintf('Error message: "%s", error code: "%s"', $error->getMessage(), $error->getCode()), 0, $previous);
    }
}