<?php

namespace LocationBundle\Exception;

use Curl\Curl;

class CurlClientConfigurationException extends \Exception
{
    /**
     * @param Curl $client
     * @param \Throwable|null $previous
     */
    public function __construct(Curl $client, \Throwable $previous = null)
    {
        parent::__construct($client->errorMessage, (int) $client->errorCode, $previous);
    }
}