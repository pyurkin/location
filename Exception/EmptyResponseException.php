<?php

namespace LocationBundle\Exception;

use Curl\Curl;

class EmptyResponseException extends \Exception
{
    /**
     * @param Curl $client
     * @param \Throwable|null $previous
     */
    public function __construct(Curl $client, \Throwable $previous = null)
    {
        parent::__construct(sprintf('Response is empty. Url "%s"', $client->url), 0, $previous);
    }
}