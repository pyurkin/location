<?php

namespace LocationBundle\Model;

class Coordinates
{
    /**
     * @var float
     */
    private $longitude;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(float $latitude, float $longitude)
    {
        if (!is_float($latitude) || !is_float($longitude)) {
            throw new \InvalidArgumentException(sprintf(
                'Both %s constructor arguments should be a float value! Given "%s" and "%s".',
                __CLASS__,
                gettype($latitude),
                gettype($longitude)
            ));
        }

        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }
}