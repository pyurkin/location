<?php

namespace LocationBundle\Model;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validation;

class Response
{
    /**
     * @var array
     */
    private $locations = [];

    /**
     * @var Error|null
     */
    private $error;

    /**
     * @var array
     */
    private $validationErrors = [];

    /**
     * @param array $response
     */
    public function __construct(array $response)
    {
        $valid = $this->validate($response, $this->getCommonConstraint(), 'Response');

        if ($valid === true) {

            if ($response['success'] === true) {
                $constraint = $this->getSuccessConstraint();
            } else {
                $constraint = $this->getFailureConstraint();
            }

            $valid = $this->validate($response['data'], $constraint, 'Response[data]');

            if ($valid === true) {
                if ($response['success'] === true) {
                    foreach ($response['data']['locations'] as $location) {
                        $this->locations[] = new Location($location['name'], new Coordinates($location['coordinates']['lat'], $location['coordinates']['long']));
                    }
                } else {
                    $this->error = new Error($response['data']['message'], (int) $response['data']['code']);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getLocationsCollection(): array
    {
        return $this->locations;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->error === null;
    }

    /**
     * @return Error|null
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return count($this->validationErrors) === 0;
    }

    /**
     * @return array
     */
    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    /**
     * @param array      $data
     * @param Constraint $constraint
     * @param string     $errorPrefix
     *
     * @return bool
     */
    private function validate(array $data, Constraint $constraint, string $errorPrefix = ''): bool
    {
        $validator = Validation::createValidator();

        $violations = $validator->validate($data, $constraint);
        foreach ($violations as $violation) {
            /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
            $this->validationErrors[] = sprintf('%s%s %s', $errorPrefix, $violation->getPropertyPath(), $violation->getMessage());
        }

        return count($violations) === 0;
    }

    /**
     * @return Constraint
     */
    private function getCommonConstraint(): Constraint
    {
        $constraint = new Constraints\Collection([
            'data' => new Constraints\Type('array'),
            'success' => new Constraints\Type('boolean')
        ]);

        return $constraint;
    }

    /**
     * @return Constraint
     */
    private function getSuccessConstraint(): Constraint
    {
        $coordinateConstraints = [
            new Constraints\NotBlank(),
            new Constraints\GreaterThan(0),
            new Constraints\Type('float'),
        ];
        $constraint = new Constraints\Collection([
            'locations' => new Constraints\All([
                new Constraints\Collection([
                    'name' => [new Constraints\NotBlank(), new Constraints\Type('string'), new Constraints\Length(['min' => 1])],
                    'coordinates' => new Constraints\Collection([
                        'lat' => $coordinateConstraints,
                        'long' => $coordinateConstraints,
                    ]),
                ]),
            ]),
        ]);

        return $constraint;
    }

    /**
     * @return Constraint
     */
    private function getFailureConstraint(): Constraint
    {
        $constraint = new Constraints\Collection([
            'message' => [new Constraints\Type('string'), new Constraints\Length(['min' => 1])],
            'code' => [new Constraints\Type('string'), new Constraints\Length(['min' => 1])],
        ]);

        return $constraint;
    }
}