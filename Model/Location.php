<?php

namespace LocationBundle\Model;

class Location
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Coordinates
     */
    private $coordinates;

    /**
     * @param string      $name
     * @param Coordinates $coordinates
     */
    public function __construct(string $name, Coordinates $coordinates)
    {
        if (!is_string($name)) {
            throw new \InvalidArgumentException(sprintf(
                    '%s argument $name should be a string! Given "%s".',
                    __CLASS__,
                    gettype($name)
            ));
        }
        $this->name = $name;
        $this->coordinates = $coordinates;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }
}