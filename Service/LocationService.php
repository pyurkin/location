<?php

namespace LocationBundle\Service;

use \Curl\Curl;
use LocationBundle\Exception\CurlClientConfigurationException;
use LocationBundle\Exception\EmptyResponseException;
use LocationBundle\Exception\ErrorResponseException;
use LocationBundle\Exception\ValidationResponseException;
use LocationBundle\Model\Location;
use LocationBundle\Model\Response;

class LocationService
{
    /**
     * @var Curl
     */
    private $client;

    /**
     * @param Curl $client
     */
    public function __construct(Curl $client)
    {
        $this->client = $client;
        $this->client->setDefaultJsonDecoder(true);
    }

    /**
     * @return Location[]
     *
     * @throws CurlClientConfigurationException
     * @throws EmptyResponseException
     * @throws ErrorResponseException
     * @throws ValidationResponseException
     */
    public function fetchLocations(): array
    {
        $result = $this->client->get([]);

        if (is_string($this->client->errorMessage) && strlen($this->client->errorMessage)) {
            throw new CurlClientConfigurationException($this->client);
        }

        if (!is_array($result) || count($result) === 0) {
            throw new EmptyResponseException($this->client);
        }

        $response = new Response($result);

        if (!$response->isValid()) {
            throw new ValidationResponseException($this->client, $response->getValidationErrors());
        }

        if (!$response->isSuccess()) {
            throw new ErrorResponseException($response->getError());
        }

        return $response->getLocationsCollection();
    }
}